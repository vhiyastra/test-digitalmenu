package com.example.vhiyastra.digitalmenu.feature.product_type;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.example.vhiyastra.digitalmenu.R;
import com.example.vhiyastra.digitalmenu.model.MenuCollection;
import com.example.vhiyastra.digitalmenu.model.ProductType;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class CategoryFragment extends Fragment implements ProductTypeAdapter.OnItemClickListener {

    List mTypeList = new ArrayList<>();
    @BindView(R.id.vp_product)
    ViewPager mVpProduct;
    @BindView(R.id.btnClose)
    ImageView mBtnClose;
    @BindView(R.id.rv_product_type)
    RecyclerView mRvProductType;
    @BindView(R.id.product_type_container)
    RelativeLayout mProductTypeContainer;

    ProductTypeAdapter mProductTypeAdapter;
    List<ProductType> mProductTypeList = new ArrayList<>();
    GridLayoutManager mLayoutManager;

    boolean isProductCategoryShown = true;

    public CategoryFragment() {
        // Required empty public constructor
    }

    public static CategoryFragment newInstance(MenuCollection menuCollection) {

        Bundle args = new Bundle();
        Gson gson = new Gson();
        String x = gson.toJson(menuCollection);
        args.putString("menu_collection", x);
        CategoryFragment fragment = new CategoryFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = getArguments();
        String x = bundle.getString("menu_collection");
        Gson gson = new Gson();

        Type listProductType = new TypeToken<MenuCollection>() {}.getType();
        mTypeList =  ((MenuCollection)gson.fromJson(x, listProductType)).getProductType();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_product_type, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mProductTypeAdapter = new ProductTypeAdapter(getContext(),this);
        mLayoutManager = new GridLayoutManager(getContext(),3);
        mRvProductType.setAdapter(mProductTypeAdapter);
        mRvProductType.setLayoutManager(mLayoutManager);

        if (mTypeList != null)
            mProductTypeAdapter.updateItems(mTypeList);
    }

    @OnClick(R.id.btnClose)
    public void onClick() {
        if(isProductCategoryShown)
        {
            mRvProductType.setVisibility(View.GONE);
            isProductCategoryShown = false;
        }
        else
        {
            mRvProductType.setVisibility(View.VISIBLE);
            isProductCategoryShown = true;
        }

    }

    @Override
    public void onItemClick(int position) {

    }
}
