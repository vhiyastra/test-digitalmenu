
package com.example.vhiyastra.digitalmenu.model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProductType {

    @SerializedName("product_type_id")
    @Expose
    private String productTypeId;
    @SerializedName("product_type_name")
    @Expose
    private String productTypeName;
    @SerializedName("products")
    @Expose
    private List<Product> products = null;

    /**
     * No args constructor for use in serialization
     * 
     */
    public ProductType() {
    }

    /**
     * 
     * @param productTypeId
     * @param productTypeName
     * @param products
     */
    public ProductType(String productTypeId, String productTypeName, List<Product> products) {
        super();
        this.productTypeId = productTypeId;
        this.productTypeName = productTypeName;
        this.products = products;
    }

    public String getProductTypeId() {
        return productTypeId;
    }

    public void setProductTypeId(String productTypeId) {
        this.productTypeId = productTypeId;
    }

    public ProductType withProductTypeId(String productTypeId) {
        this.productTypeId = productTypeId;
        return this;
    }

    public String getProductTypeName() {
        return productTypeName;
    }

    public void setProductTypeName(String productTypeName) {
        this.productTypeName = productTypeName;
    }

    public ProductType withProductTypeName(String productTypeName) {
        this.productTypeName = productTypeName;
        return this;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public ProductType withProducts(List<Product> products) {
        this.products = products;
        return this;
    }

}
