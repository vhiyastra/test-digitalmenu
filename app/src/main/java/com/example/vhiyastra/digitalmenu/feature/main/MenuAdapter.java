package com.example.vhiyastra.digitalmenu.feature.main;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.vhiyastra.digitalmenu.model.MenuCollection;
import com.example.vhiyastra.digitalmenu.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vhiyastra on 1/18/2017.
 */

public class MenuAdapter extends RecyclerView.Adapter<MenuAdapter.MenuViewHolder> {

    private List<MenuCollection> mList = new ArrayList<>();
    private int rowLayout;
    private Context mContext;
    private OnItemClickListener mOnItemClickListener;

   public interface OnItemClickListener{
        void onItemClick(int position);
    }

    public class MenuViewHolder extends RecyclerView.ViewHolder {
        LinearLayout menuLayout;
        TextView menuTitle;
        public MenuViewHolder(View v) {
            super(v);
            menuLayout = (LinearLayout) v.findViewById(R.id.menuLayout);
            menuTitle  = (TextView) v.findViewById(R.id.menu_item_title);
        }
    }

    public void updateData(List<MenuCollection> newList){
        mList.clear();
        mList.addAll(newList);
        notifyDataSetChanged();
    }

    public MenuCollection getMenuCollectionAt(int position)
    {
        return mList.get(position);
    }

    public MenuAdapter( int rowLayout, Context mContext, OnItemClickListener onItemClickListener) {
        this.mOnItemClickListener = onItemClickListener;
        this.rowLayout = rowLayout;
        this.mContext = mContext;
    }

    @Override
    public MenuAdapter.MenuViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(rowLayout, parent, false);
        return new MenuViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MenuAdapter.MenuViewHolder holder, final int position) {
        holder.menuTitle.setText(mList.get(position).getProductCategoryName());
        holder.menuLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mOnItemClickListener.onItemClick(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }
}
