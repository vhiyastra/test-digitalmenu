package com.example.vhiyastra.digitalmenu.feature.product_type;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.vhiyastra.digitalmenu.R;
import com.example.vhiyastra.digitalmenu.model.ProductType;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by vhiyastra on 1/20/2017.
 */
public class ProductTypeViewHolder extends RecyclerView.ViewHolder{

    @BindView(R.id.lblTitle)
    TextView mLblTitle;
    Spinner mSpinnerTitle;
    List<String> list;
    ArrayAdapter<String> spinnerArrayAdapter;

    public ProductTypeViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public void bind(Context context, final ProductType model, final ProductTypeAdapter.OnItemClickListener listener) {
        mLblTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(getLayoutPosition());
            }
        });
        mLblTitle.setText(model.getProductTypeName());

        mSpinnerTitle = (Spinner) itemView.findViewById(R.id.spinner_title);

        list = new ArrayList<String>();
        list.add(model.getProductTypeName());
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(context,android.R.layout.simple_spinner_item,list);
        mSpinnerTitle.setAdapter(spinnerArrayAdapter);
    }
}
