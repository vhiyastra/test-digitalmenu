package com.example.vhiyastra.digitalmenu.network;

import com.example.vhiyastra.digitalmenu.model.MenuCollection;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by vhiyastra on 1/18/2017.
 */

public interface ApiInterface {

    @GET("api/get_menu_collection")
    Call<List<MenuCollection>> getMenuCollection();

}
