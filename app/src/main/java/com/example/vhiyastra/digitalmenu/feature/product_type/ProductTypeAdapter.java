package com.example.vhiyastra.digitalmenu.feature.product_type;

import android.content.Context;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.vhiyastra.digitalmenu.model.ProductType;
import com.example.vhiyastra.digitalmenu.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;

/**
 * Created by vhiyastra on 1/20/2017.
 */


public class ProductTypeAdapter extends RecyclerView.Adapter<ProductTypeViewHolder> {

    private static final String TAG = ProductTypeAdapter.class.getSimpleName();

    private Context mContext;
    private List<ProductType> mList = new ArrayList<>();
    private OnItemClickListener mOnItemClickListener;

    public ProductTypeAdapter(Context context, OnItemClickListener onItemClickListener) {
        this.mContext = context;
        this.mOnItemClickListener = onItemClickListener;
    }

    public void updateItems(final List<ProductType> newItems) {
        final List<ProductType> oldItems = new ArrayList<>(this.mList);

        if (newItems != null) {
            this.mList.addAll(newItems);
        }
        DiffUtil.calculateDiff(new DiffUtil.Callback() {
            @Override
            public int getOldListSize() {
                return oldItems.size();
            }

            @Override
            public int getNewListSize() {
                return mList.size();
            }

            @Override
            public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
                return oldItems.get(oldItemPosition).equals(newItems.get(newItemPosition));
            }

            @Override
            public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
                return oldItems.get(oldItemPosition).equals(newItems.get(newItemPosition));
            }
        }).dispatchUpdatesTo(this);

    }

    @Override
    public ProductTypeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View view = inflater.inflate(R.layout.item_product_type, parent, false);
        ButterKnife.bind(this, view);

        return new ProductTypeViewHolder(view);
    }


    @Override
    public void onBindViewHolder(ProductTypeViewHolder holder, int position) {
        ProductType item = mList.get(position);

        //Todo: Setup viewholder for item
        holder.bind(mContext, item, mOnItemClickListener);
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public interface OnItemClickListener {
        void onItemClick(int position);
    }


}
