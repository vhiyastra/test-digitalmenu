package com.example.vhiyastra.digitalmenu.feature.main;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Spinner;

import com.example.vhiyastra.digitalmenu.R;
import com.example.vhiyastra.digitalmenu.feature.product_type.CategoryFragment;
import com.example.vhiyastra.digitalmenu.feature.shared.BaseActivity;
import com.example.vhiyastra.digitalmenu.model.MenuCollection;
import com.example.vhiyastra.digitalmenu.network.ApiClient;
import com.example.vhiyastra.digitalmenu.network.ApiInterface;

import java.util.ArrayList;
import java.util.List;

import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends BaseActivity implements MenuAdapter.OnItemClickListener {

    private static final String TAG = MainActivity.class.getSimpleName();
    List<MenuCollection> mCategory = new ArrayList<>();
    MenuAdapter mAdapter;
    Spinner mSpinnerTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        ArrayAdapter<String> adapter;
//        List<String> list;
//
//        mSpinnerTitle = (Spinner) findViewById(R.id.spinner_title);
//        list = new ArrayList<>();
//        list.add("Data 1");
//        list.add("Data 2");
//        list.add("Data 3");
//        list.add("Data 4");
//        list.add("Data 5");
//        adapter = new ArrayAdapter<>(getApplicationContext(),
//                android.R.layout.simple_spinner_dropdown_item, list);
//        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        mSpinnerTitle.setAdapter(adapter);

        final RecyclerView recyclerView = (RecyclerView) findViewById(R.id.category_recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        mAdapter = new MenuAdapter(R.layout.item_product_category, this, this);
        recyclerView.setAdapter(mAdapter);

        ApiInterface apiService =
                ApiClient.getMenuCollection().create(ApiInterface.class);

        Call<List<MenuCollection>> call = apiService.getMenuCollection();
        call.enqueue(new Callback<List<MenuCollection>>() {
            @Override
            public void onResponse(Call<List<MenuCollection>> call, Response<List<MenuCollection>> response) {
                mAdapter.updateData(response.body());
            }

            @Override
            public void onFailure(Call<List<MenuCollection>> call, Throwable t) {
                // Log error here since request failed
                Log.e(TAG, t.toString());
            }
        });

    }

    @Override
    public void onItemClick(int position) {

        MenuCollection menuCollection = mAdapter.getMenuCollectionAt(position);

        getSupportFragmentManager().beginTransaction().replace(R.id.frame_layout, CategoryFragment.newInstance(menuCollection)).commit();

    }

    @OnClick(R.id.spinner_title)
    public void onClick() {
    }
}
