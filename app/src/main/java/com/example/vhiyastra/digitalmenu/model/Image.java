
package com.example.vhiyastra.digitalmenu.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Image {

    @SerializedName("product_image_id")
    @Expose
    private String productImageId;
    @SerializedName("image_url")
    @Expose
    private String imageUrl;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Image() {
    }

    /**
     * 
     * @param imageUrl
     * @param productImageId
     */
    public Image(String productImageId, String imageUrl) {
        super();
        this.productImageId = productImageId;
        this.imageUrl = imageUrl;
    }

    public String getProductImageId() {
        return productImageId;
    }

    public void setProductImageId(String productImageId) {
        this.productImageId = productImageId;
    }

    public Image withProductImageId(String productImageId) {
        this.productImageId = productImageId;
        return this;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public Image withImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
        return this;
    }

}
