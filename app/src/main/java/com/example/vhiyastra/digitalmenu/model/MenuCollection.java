
package com.example.vhiyastra.digitalmenu.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MenuCollection {

    @SerializedName("product_category_id")
    @Expose
    private String productCategoryId;
    @SerializedName("product_category_name")
    @Expose
    private String productCategoryName;
    @SerializedName("type")
    @Expose
    private List<ProductType> mProductType = null;

    /**
     * No args constructor for use in serialization
     * 
     */
    public MenuCollection() {
    }

    /**
     * 
     * @param productCategoryName
     * @param productType
     * @param productCategoryId
     */
    public MenuCollection(String productCategoryId, String productCategoryName, List<ProductType> productType) {
        super();
        this.productCategoryId = productCategoryId;
        this.productCategoryName = productCategoryName;
        this.mProductType = productType;
    }

    public String getProductCategoryId() {
        return productCategoryId;
    }

    public void setProductCategoryId(String productCategoryId) {
        this.productCategoryId = productCategoryId;
    }

    public MenuCollection withProductCategoryId(String productCategoryId) {
        this.productCategoryId = productCategoryId;
        return this;
    }

    public String getProductCategoryName() {
        return productCategoryName;
    }

    public void setProductCategoryName(String productCategoryName) {
        this.productCategoryName = productCategoryName;
    }

    public MenuCollection withProductCategoryName(String productCategoryName) {
        this.productCategoryName = productCategoryName;
        return this;
    }

    public List<ProductType> getProductType() {
        return mProductType;
    }

    public void setProductType(List<ProductType> productType) {
        this.mProductType = productType;
    }

    public MenuCollection withType(List<ProductType> productType) {
        this.mProductType = productType;
        return this;
    }

}
